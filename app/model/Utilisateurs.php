<?php
/**
* Définition modèle
*/
class Utilisateurs extends \Illuminate\Database\Eloquent\Model {
  protected $primaryKey = "id";
  protected $table = "utilisateurs";
  public $timestamps = false;

    function billet() {
        return $this->belongsTo('\Billet');
    }

    function commentaire() {
        return $this->belongsTo('\Commentaire');
    }
}
?>