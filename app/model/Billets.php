<?php
/**
* Définition modèle
*/
class Billets extends \Illuminate\Database\Eloquent\Model {
  protected $primaryKey = "id";
  protected $table = "billets";
  public $timestamps = false;

  public function utilisateur() {
    return $this->belongsTo('\Utilisateurs', 'id_utilisateur');
  }

  public function categories() {
    return $this->belongsTo('\Categories', 'id_categorie');
  }

  function commentaires() {
    return $this->hasMany('\Commentaire', 'id_billet');
  }
}
?>