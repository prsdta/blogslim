<?php
/**
* Définition modèle
*/
class Commentaires extends \Illuminate\Database\Eloquent\Model {
  protected $primaryKey = "id";
  protected $table = "commentaires";
  public $timestamps = false;

  function billet() {
    return $this->hasOne('\Billet', 'id_billet');
  }

  function utilisateur(){
    return $this->hasOne('\Utilisateur', 'id_utilisateur');
  }
}
?>