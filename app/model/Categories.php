<?php
/**
* Définition modèle
*/
class Categories extends \Illuminate\Database\Eloquent\Model {
  protected $primaryKey = "id";
  protected $table = "categories";
  public $timestamps = false;

  function billet() {
    return $this->belongsTo('\Billet');
  }
}
?>