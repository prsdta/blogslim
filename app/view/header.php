<!DOCTYPE html>
<html lang="fr">
  <head >
    <title>Mon super titre</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="scale=1.0" />
    <link rel="stylesheet" href="https://cdn.rawgit.com/doximity/vital/master/releases/v1.0.1/stylesheets/vital.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $app->urlFor('root'); ?>/static/style.css"
     type="text/css" media="screen" />
  </head>
  <body>
    <div class="row header">
      <section class="section">
        <nav>
          <ul class="menu">
            <li><a href="<?php echo $app->urlFor('root');?>">Accueil</a></li>
            <?php
            if (empty($_SESSION['pseudo'])) {
              echo '<li><a href="' . $app->urlFor('root') . 'login">Se Connecter</a></li>';
              echo '<li><a href="' . $app->urlFor('root') . 'register">S\'inscrire</a></li>';
            } else {
              if ($_SESSION['profil'] != '') {
              echo '<li><a href="' . $app->urlFor('root') . 'redaction">Ecrire un billet</a></li>';
              }
                if ($_SESSION['profil']=='admin'){
                echo '<li><a href="' . $app->urlFor('root') . 'param_blog">Paramètres du blog</a></li>';
              }
              echo '<li><a href="' . $app->urlFor('root') . 'settings">Paramètres utilisateurs</a></li>';
              echo '<li><a href="' . $app->urlFor('root') . 'logout">Se déconnecter</a></li>';
              echo '<li><strong> Bienvenue ' . ($_SESSION['pseudo']) . '</strong></li>';
              
            }
            ?>
          </ul>
        </nav>
      </section>
    </div>

    <?php
    if (!empty($flash['info']))
      echo <<<YOP
    <div class="info row notice">
      {$flash['info']}
    </div>
YOP
?>

    <div class="section">
      <div class="categories col-1-4">
        <ul>
          <?php
          foreach ($categories as $value) {
            echo "<li> + <a href=\"" . $app->urlFor('root') .
                        "$value->label\">$value->label</a></li> ";
          }
          ?>
        </ul>
      </div>
