<section class="section padded">
  <h1>Veuillez vous connecter</h1>
  <?php
  if (!empty($flash['loginRes'])) {
    echo '<p class="login_fail row notice">' . $flash['loginRes'] . '.</p>';
  }
  ?>

  <form class="" action="login" method="post">
    <label>Nom d'utilisateur<input name="username" type="text" /></label>
    <br />
    <label>Mot de passe<input name="mdp" type="password" /></label>
    <br />
    <button class="btn small" type="submit">Se connecter</button>
  </form>
</section>
