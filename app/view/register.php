<section class="section">
  <h1>Créez votre compte :</h1>
  <p>Merci de remplir tous les champs suivants :</p>
  <?php
  if (!empty($flash['formInfo'])) {
    echo '<p class="row notice">' . $flash['formInfo'] . '.</p>';
  }
  ?>
  <p>
    <form class="full-width-forms padded" method="post">
      <div class="col">
        <label for="pseudo_id">Pseudo</label>
        <input id="pseudo_id" type="text" name="pseudo" placeholder="Votre pseudo">
      </div>

      <div class="col">
        <label for="nom_id">Nom</label>
        <input id="nom_id" type="text" name="nom" placeholder="Votre nom">
      </div>

      <div class="col">
        <label for="prenom_id">Prénom</label>
        <input id="prenom_id" type="text" name="prenom" placeholder="Votre prénom">
      </div>

      <div class="col">
        <label for="mail_id">Mail</label>
        <input id="mail_id" type="email" name="mail" placeholder="Votre e-mail">
      </div>

      <div class="col">
        <label for="mdp_id">Mot de passe</label>
        <input id="mpd_id" type="password" name="mdp" minlength="8">
      </div>

      <div class="section row">
        <button class="btn" type="submit">S'inscrire</button>
      </div>
    </form>
  </p>
</section>
