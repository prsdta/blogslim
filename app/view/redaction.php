<section class="section">
<h2>Veuillez écrire un billet</h2>
<p>Merci de remplir tous les champs</p>

  <?php
  if (!empty($flash['formInfo'])) {
    echo '<p class="row notice">' . $flash['formInfo'] . '.</p>';
  }
  ?>

<p>
<form class="full-width-forms padded" method="post">
	<div class ="col">
		<label for="titre_id">Titre</label>
		<input id="titre_id" type="text" name="titre" placeholder="Titre du billet">
	</div>

	<div>
		<label for"message_id">Message</label>
		<textarea name="message" placeholder="Contenu du message"> </textarea>
	</div>

	<div>
		 <label for"categorie_id">Catégorie</label>
		 <select name="categorie">
		 		<?php
					foreach(Categories::all() as $value)
					{
		 		echo '<option value="'. $value->id.'">'.$value->label.'</option>';
					}
		 		?>
		 		
		 </select>

	<div class="section row">
		<button class="btn" type="submit">Valider</button>
	</div>
</form>
</p>
</section>