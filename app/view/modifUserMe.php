<h1>Modifier mes informations</h1>

<section class="section">
  <?php
  if (!empty($flash['formInfo'])) {
    echo '<p class="row notice">' . $flash['formInfo'] . '.</p>';
  }
  ?>
  <p>
    <form class="full-width-forms padded" method="post">
      <div class="col">
        <label for="pseudo_id">Modifier pseudo</label>
        <input id="pseudo_id" type="text" name="pseudo" value="<?php echo $_SESSION['pseudo']; ?>">
      </div>
      <div class="col">
        <label for="mail_id">Mail</label>
        <input id="mail_id" type="email" name="mail" value="<?php echo $user->mail; ?>">
      </div>

      <div class="col">
        <label for="mdp_id">Ancien mot de passe</label>
        <input id="mpd_id" type="password" name="mdpold" minlength="8">
      </div>
      <div class="col">
        <label for="mdp_id">Nouveau mot de passe</label>
        <input id="mpd_id" type="password" name="mdpnew" minlength="8">
      </div>
      <div class="col">
        <label for="mdp_id">Retapez le nouveau mot de passe</label>
        <input id="mpd_id" type="password" name="mdpnew2" minlength="8">
      </div>

      <div class="section row">
        <button class="btn" type="submit">Valider</button>
      </div>
    </form>
  </p>
</section>
