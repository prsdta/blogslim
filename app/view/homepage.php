<div class="main col-3-4">
  <h1>Bienvenue sur le blog !</h1>

  <?php
  foreach ($billets as $value) {
    $message = substr($value->message, 0, 150);

    echo <<<YOP
      <h2>{$value->titre}</h2>
      <p><em>Ecrit le : {$value->date}</em></p>
      <p>Par : {$value->author_name}</p>
      <p><strong>{$value->category_label}</strong></p>
      <p>{$message}...</p>
      <a href="/item/{$value->id}">Lire la suite</a> </p>
YOP;
}
  ?>
  
</div>
</div>
<section class="row pagination">
  <?php
    // Affichage de boutons pour naviguer entre les pages
    // prévoir une manière de les délencher et la numérotation des pages
    /*
     if ($check) {
       echo '<a class="previous_page" href="'. $app->urlfor('root') . $page-- .
            '">← Page précédente</a>';
     }
     if ($check) {
       echo '<a class="next_page" href="' . $app->urlfor('root') . $page++ .
       '">Page suivante →</a>';
     }
     */
  ?>
</section>