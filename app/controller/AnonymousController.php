<?php
require_once 'vendor/autoload.php';
use \conf\Authentication;

class AnonymousController extends Controller {

  public function header() {
    $app = Controller::$app;
    $categories = Categories::all();
    Controller::$app->render('header.php', compact('app', 'categories'));
  }

  public function footer() {
    Controller::$app->render('footer.php');
  }

  public function index($id = 1){
    // On récupère les 20 derniers billets, avec le nombre de pages-1 à skip
    $billets = Billets::orderBy('date', 'desc')->skip(20*($id-1))->take(20)->get();
    foreach($billets as $billet) {
      // Récupération de la catégorie du billet
      $category = Categories::where('id', '=', $billet['id_categorie'])->first();
      $billet['category_label'] = $category->label;
      // Récupération du nom de l'auteur
      $author = Utilisateurs::where('id', '=', $billet->id_utilisateur)->first();
      $billet['author_name'] = "$author->nom $author->prenom";

      $comment = Commentaires::where('id_billet', '=', $billet->id)->get();
      $messages = array();
      $auteur = array();
      foreach($comment as $comms) {
        $username = Utilisateurs::where('id', '=', $comms->id_utilisateur)->first();
        array_push($messages, $comms->message);
        array_push($auteur, $username->pseudo);
       /* $billet['mess'] = $comms->message;
        $billet['aut'] = $username->pseudo;*/
      }
    $billet['comm'] = $messages;
    $billet['aut'] = $auteur;
    }

    $this->header();
    Controller::$app->render('homepage.php', compact('billets'));
    $this->footer();
  }

  public function affUserID($id){
    $this->header();
   $user = Utilisateurs::find($id);
    if ($user != NULL){
      Controller::$app->render('aff_user.php', compact('user'));
    }
    $this->footer();
  }

  public function modifUser($id){
    $app = Controller::$app;
    $util = Utilisateurs::find($id);
    $params = $app->request->params('radie');
    if (empty($params)) {
        $util->radie = 0;
        $util->save();
        $app->flash('info', 'L\'utilisateur a bien été rehabilité.');
    }
    if (!empty($params)) {
     $util->radie = 1;
      $util->save();
      $app->flash('info', 'L\'utilisateur a bien été radié.');
    }
    $app->redirect('/param_blog');

  }

  /** Pour afficher la page d'inscription */
  public function register(){
    $this->header();
    Controller::$app->render('register.php');
    $this->footer();
  }


  /** Pour ajouter un utilisateur à la DB */
  public function createUser(){
    $app = Controller::$app;
    // Créée un nouvel objet user
    $user = new Utilisateurs;

    // Si l'utilisateur n'existe pas déjà
    if (Utilisateurs::where('pseudo', '=', $app->request->post('pseudo'))
        ->first() == NULL) {
      // On récupère les valeurs du post
      $user->pseudo = $app->request->post('pseudo');
      $user->nom = $app->request->post('nom');
      $user->prenom = $app->request->post('prenom');
      $user->mail = $app->request->post('mail');
      $user->mdp = Authentication::passwordHash( $app->request->post('mdp') );
      $user->profil = 'membre';
      $user->radie = 0;
      // Ecrit les valeurs de l'objet dans la DB
      $user->save();
      $app->flash('info', 'Vous êtes bien enregistré, vous pouvez vous controller.');
      $app->redirect('/login');
    }else {
      $app->flash('info', 'Ce pseudonyme est déjà pris.');
    }
  }
  
  public function updateMe(){
    $app = Controller::$app;
    $util = Utilisateurs::find($_SESSION['id']);
    if($util->radie == 0){
      if($app->request->post('pseudo') != $util->pseudo ){
        $util->pseudo = $app->request->post('pseudo');
      }
      if($app->request->post('mail') != $util->mail ){
    $util->mail = $app->request->post('mail');
      }
    $mdpold = $app->request->post('mdpold');
    $mdpnew = $app->request->post('mdpnew');
    $mdpnew2 = $app->request->post('mdpnew2');
    
     if (Authentication::authenticate($user, $mdpold)) {
      if ($mdpnew == $mdpnew2){
        $user->mdp = Authentication::passwordHash($mdpnew);
        $app->save;
        $app->flash('info', 'Vos informations ont bien été modifées');
        $app->redirect('/');
      }
    }
  }
  }
  public function affMe(){
    $this->header();
    $user = Utilisateurs::find($_SESSION['id']);
    Controller::$app->render('modifUserMe.php', compact('user'));
    $this->footer();
  }

  /** Methode pour afficher l'écran de login
   *  @param fail appeler la méthode avec true si l'utilisateur
   *              n'a pas pu se connecter
   */
  public function loginPrompt() {
    $this->header();
    Controller::$app->render('login.php');
    $this->footer();
  }

  /** Méthode pour connecter un utilisateur après loginPrompt */
  public function login() {
    $this->header();
    $app = Controller::$app;

    // Réception de la request
    $user = $app->request->post('username');
    $mdp =$app->request->post('mdp');

    if (Authentication::authenticate($user, $mdp)) {
      // Si authentifié, on réinitialise la session
      $_SESSION = array();

      $user = Utilisateurs::where('pseudo', '=', $user)->first();
      $_SESSION['pseudo'] = $user->pseudo;
      $_SESSION['profil'] = $user->profil;
      $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];

      $app->flash('info', "Vous êtes maintenant connecté !");
      $app->redirect('/');
    }else {
      // Sinon retour à l'écran de connection
      $app->flash('loginRes', "Vous n'avez pas pu être identifié");
      $app->redirect('/login');
    }

    $this->footer();
  }

  /** Méthode pour afficher la page d'enregistrement du billet */
   public function redaction(){
    $this->header();
    Controller::$app->render('redaction.php');
    $this->footer();
  }

  /** Méthode pour écrire un billet */

  public function createBillet(){
    $this->header();
    $app = Controller::$app;
    $user = Utilisateurs::where('pseudo', '=', $_SESSION['pseudo'])->first();

    $billet = new Billets;

    $billet->titre = $app->request->post('titre');
    $billet->message = $app->request->post('message');
    $billet->id_utilisateur = $user->id;
    $billet->id_categorie = $app->request->post('categorie');

    $billet->save();
    $app->flash('info', 'Votre billet a bien été enregistré.');
    $app->redirect('/');

    $this->footer();

}

  /** Méthode pour afficher un billet */
public function affiche_billet($id){
    $this->header();

    $billet = Billets::find($id);
    if ($billet != NULL){
      $auteur = $billet->utilisateur;
      $billet['auteur'] = "$auteur->prenom $auteur->nom";

      $billet['categorie'] = $billet->categories->label;

      $comment = Commentaires::where('id_billet', '=', $id)->get();
      $comments_data[] = [];
      foreach($comment as $comm) {
          $user = Utilisateurs::where('id', '=', $comm->id_utilisateur)->first();
          // Rajoute une entrée au tableau comms
          array_push($comments_data, array($comm->message, $user->pseudo));
      }
      array_shift($comments_data);
      $billet['comm'] = $comments_data;
      Controller::$app->render('aff_billet.php', array('billet' => $billet,
                                                        'comm' => $comments_data));
      if ($_SESSION['profil'] != '') {
        Controller::$app->render('comm_form.php');
      }
    }else
      $app->notFound();

    $this->footer();
}

    // public function redaction_billet($id){
    //   $this->header();
    //   $this->write_comm($id);
    //   Controller::$app->render('homepage.php');
    //   $this->footer();
    // }

    public function write_comm($id){


        $app = Controller::$app;
        $user = Utilisateurs::where('pseudo', '=', $_SESSION['pseudo'])->first();

        $comm = new Commentaires;

        $comm->message = $app->request->post('message');

    $comm->id_utilisateur = $user->id;
    $comm->id_billet = $id;

    $comm->save();
    $app->flash('info', 'Votre commentaire a bien été enregistré.');
    $app->redirect('/');

  }

    /** Pour afficher la page d'inscription */
  public function affParam(){
    $this->header();
    $users = Utilisateurs::all();
    Controller::$app->render('param_blog.php', compact('users'));
    $this->footer();
  }

  /** Pour ajouter une categorie à la DB */
  public function createCat(){
    $app = Controller::$app;
    // Créée un nouvel objet categorie
    $cat = new Categories;

    // Si la categorie n'existe pas déjà
    if (Categories::where('label', '=', $app->request->post('label'))
        ->first() == NULL) {
      // On récupère les valeurs du post
      $cat->label = $app->request->post('label');
      // Ecrit les valeurs de l'objet dans la DB
      $cat->save();
      $app->flash('info', 'La nouvelle catégorie a bien été ajoutée');
      $app->redirect('/');
    }else {
      $app->flash('info', 'Cette catégorie existe déjà.');
    }
  }


  public function logout() {
    // vide le tableau
    $_SESSION = array();
    session_destroy();
    Controller::$app->redirect('/');
  }

  public function insert_info(){
    $app = Controller::$app;
    $nom = $app->request->post('nom');
    $app->flash('info', "J'ai ajouté le nom « $nom »");
    $app->redirectTo('root');
  }
}

?>
