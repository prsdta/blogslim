<?php
// Autoloader inclusion
require_once 'vendor/autoload.php';

// Accesses
use conf\Router;
use conf\Configuration;
use Slim\Slim;

// DB configuration
Configuration::config();

// Slim app creation
$app = new Slim(array(
  'templates.path' => 'app/view',
  'debug' => true,
  'log.enabled' => true
));

// Durée des cookies de session à 7 jours max
session_start(array('cookie_lifetime' => 604800));

// Now routing definition
$router = new Router($app, 'conf/routes', '');
$router->parseRoutes();

// Finally, generate result
$app->run();
