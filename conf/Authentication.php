<?php

namespace conf;
require_once 'vendor/autoload.php';
use \Utilisateurs;

class Authentication {

  public static function authenticate($username, $password) {
    $res = false;
    $user = Utilisateurs::where('pseudo', '=', $username)->first();
    // Condition à changer
    if ($user != NULL) {
      // On compare les deux hashes
      if ($user->mdp == Authentication::passwordHash($password) && !$user->radie) {
          $res = true;
        }
    }
    return $res;
  }

  public static function logout() {
    session_destroy();
  }

  public static function isAuthenticated() {
    return $_SESSION['pseudo'] != NULL;
  }

  public static function getUser() {
    return Utilisateurs::where('pseudo', $_SESSION['pseudo']);
  }

  public static function passwordHash($password) {
    $salt = hash('sha256', "DUT AS Charlemagne");
    return password_hash("{$password}", PASSWORD_BCRYPT, array('salt' => $salt));
  }
}
